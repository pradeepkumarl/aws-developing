package com.classpathio.sqsdemo.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.classpathio.sqsdemo.model.Employee;
import com.classpathio.sqsdemo.service.SqsService;

@Component
public class BootstrapSQSClient implements CommandLineRunner{

	@Autowired
	private SqsService sqsService;
	
	@Override
	public void run(String... args) throws Exception {
		
		Employee vishnu = new Employee(12, "vishnu", 36);

		this.sqsService.sendMessage(vishnu.toString());
	}

}
