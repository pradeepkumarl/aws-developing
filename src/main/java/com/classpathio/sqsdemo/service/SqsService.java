package com.classpathio.sqsdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.SendMessageRequest;

@Service
public class SqsService {
	
	@Autowired
	private AmazonSQS amazonSQS;
	
	public void sendMessage(String message) {
		System.out.println("Inside the SQS service ::::::::::");
		SendMessageRequest messageRequest = new SendMessageRequest()
													.withQueueUrl("https://sqs.ap-south-1.amazonaws.com/831955480324/messages")
													.withMessageBody(message);
													//.addMessageAttributesEntry("message", new MessageAttributeValue().withStringValue("hello-world"));
		
			amazonSQS.sendMessage(messageRequest);
			
	}

}
