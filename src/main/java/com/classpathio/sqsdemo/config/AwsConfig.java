package com.classpathio.sqsdemo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.profile.internal.BasicProfile;
import com.amazonaws.auth.profile.internal.ProfileStaticCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;

@Configuration
public class AwsConfig {
	
	
	@Bean
	public BasicAWSCredentials awsCredentials() {
		return new BasicAWSCredentials("", "");
	}
	

	
	@Bean
	public AmazonSQS amazonSql() {
		return AmazonSQSClientBuilder
					.standard()
					.withCredentials(new AWSStaticCredentialsProvider(awsCredentials()))
					.withRegion(Regions.AP_SOUTH_1)
					.build();
	}

}
